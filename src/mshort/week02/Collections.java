package mshort.week02;

import java.util.*;

public class Collections {

    public static void main(String[] args) {
        // List Example
        System.out.println("=== List Demo ===");

        List<String> bestSellingList = new ArrayList<>();
        bestSellingList.add("Mario Kart 8 Deluxe");
        bestSellingList.add("Animal Crossing: New Horizons");
        bestSellingList.add("Super Smash Bros. Ultimate");
        bestSellingList.add("The Legend of Zelda: Breath of the Wild");
        bestSellingList.add("Pokemon Sword and Shield");

        System.out.println("Best Selling Nintendo Switch Games of Summer 2020:");
        System.out.println();
        int iLineNumber = 1;
        for (Object gameTitle : bestSellingList) {
            System.out.println( iLineNumber + ". " + gameTitle);
            iLineNumber = iLineNumber + 1;
        }
        System.out.println();


        // Set Example (No Duplicates Allowed)
        System.out.println("=== Set Demo ===");
        System.out.println("A Set cannot contain duplicate Objects.");
        System.out.println("We will add to our set the word for each number in the phone number for the BYU-I helpdesk.");
        System.out.println("Number: 2084961411, Although we add 10 items, the Set will not contain duplicates.");
        Set<String> mySet = new TreeSet<>();
        mySet.add("two");
        mySet.add("zero");
        mySet.add("eight");
        mySet.add("four");
        mySet.add("nine");
        mySet.add("six");
        mySet.add("one");
        mySet.add("four");
        mySet.add("one");
        mySet.add("one");

        System.out.println();
        System.out.println("Set Contents:  [" + mySet.size() + " Objects]");
        for (Object strDigit : mySet) {
            System.out.println((String) strDigit);
        }
        System.out.println();


        // LinkedList Example
        System.out.println("=== LinkedList Demo ===");
        System.out.println("We will add to our LinkedList the word for each number in the phone number for the BYU-I helpdesk.");
        System.out.println("The Number is : 2084961411, Objects are added in order.");
        System.out.println("We will traverse the list to display it without modifying the list.");
        LinkedList<String> myLinkedList = new LinkedList<>();
        myLinkedList.add("two");
        myLinkedList.add("zero");
        myLinkedList.add("eight");
        myLinkedList.add("four");
        myLinkedList.add("nine");
        myLinkedList.add("six");
        myLinkedList.add("one");
        myLinkedList.add("four");
        myLinkedList.add("one");
        myLinkedList.add("one");
        System.out.println();

        System.out.println("LinkedList Contents: [" + myLinkedList.size() + " Objects]");
        Iterator<String> myLinkedListIterator = myLinkedList.iterator();
        while (myLinkedListIterator.hasNext()) {
            System.out.println(myLinkedListIterator.next());
        }
        System.out.println("LinkedList Size: [" + myLinkedList.size() + " Objects]");
        System.out.println();
        System.out.println("Now we will traverse the list and remove all instances of \"four\".");
        int iIndex = 0;
        while (iIndex < myLinkedList.size()) {
            if (Objects.equals(myLinkedList.get(iIndex), "four")) {
                myLinkedList.remove(iIndex);
                // Start Over after remove, size has changed
                iIndex = 0;
            } else {
                iIndex = iIndex + 1;
            }
        }
        System.out.println();
        System.out.println("LinkedList Size: [" + myLinkedList.size() + " Objects]");
        myLinkedListIterator = myLinkedList.iterator();
        while (myLinkedListIterator.hasNext()) {
            System.out.println(myLinkedListIterator.next());
        }
        System.out.println();

        // PriorityQueue Example
        System.out.println("=== PriorityQueue Demo ===");
        System.out.println("We will add to our queue the word for each number in the phone number for the BYU-I helpdesk.");
        System.out.println("The Number is : 2084961411, Although we add 10 Objects in a particular order, the PriorityQueue will keep things sorted.");
        System.out.println("We will remove and display each Object in the queue until we have gone through entire Queue.");
        PriorityQueue<String> myQueue = new PriorityQueue<>();
        myQueue.add("two");
        myQueue.add("zero");
        myQueue.add("eight");
        myQueue.add("four");
        myQueue.add("nine");
        myQueue.add("six");
        myQueue.add("one");
        myQueue.add("four");
        myQueue.add("one");
        myQueue.add("one");
        System.out.println();

        System.out.println("PriorityQueue Contents: [" + myQueue.size() + " Objects] (Objects in PriorityQueue are sorted)");
        Iterator<String> myQueueIterator = myQueue.iterator();
        while (myQueueIterator.hasNext()) {
            System.out.println(myQueue.poll());
        }
        System.out.println("PriorityQueue Size: [" + myQueue.size() + " Objects]");
        System.out.println();


        // Map (HashMap / TreeMap) Examples
        System.out.println("=== HashMap Demo ===");
        System.out.println("HashMap mapping stores key : value pairs as an unordered collection");
        Map<String, Integer> myMap = new HashMap<>();
        myMap.put("zero", 0);
        myMap.put("two", 2);
        myMap.put("four", 4);
        myMap.put("six", 6);
        myMap.put("eight", 8);
        myMap.put("one", 1);
        myMap.put("three", 3);
        myMap.put("five", 5);
        myMap.put("seven", 7);
        myMap.put("nine", 9);

        System.out.println();
        System.out.println("HashMap Contents: [" + myMap.size() + "] mappings");
        Set mapSet = myMap.entrySet();
        Iterator myMapIterator = mapSet.iterator();
        if (myMapIterator.hasNext()) {
            do {
                Map.Entry nextPair = (Map.Entry) myMapIterator.next();
                System.out.println("[" + nextPair.getKey() + " : " + nextPair.getValue() + "]");
            } while (myMapIterator.hasNext());
        }
        System.out.println();


        System.out.println("=== TreeMap Demo ===");
        System.out.println("TreeMap mapping stores key : value pairs as a sorted ascending ordered collection");
        System.out.println("We will put our values into the map out of order, and they will be ordered when we traverse the map to print it");
        Map<Integer, String> myTreeMap = new TreeMap<>();
        myTreeMap.put(7, "seven");
        myTreeMap.put(2, "two");
        myTreeMap.put(9, "nine");
        myTreeMap.put(0, "zero");
        myTreeMap.put(8, "eight");
        myTreeMap.put(1, "one");
        myTreeMap.put(3, "three");
        myTreeMap.put(5, "five");
        myTreeMap.put(6, "six");
        myTreeMap.put(4, "four");

        System.out.println();
        System.out.println("TreeMap Contents: [" + myTreeMap.size() + " mappings] (TreeMap Collections are in ascending order)");
        int iMapIndex = 0;
        while (iMapIndex < myTreeMap.size()) {
            System.out.println(myTreeMap.get(iMapIndex));
            iMapIndex = iMapIndex + 1;
        }
        System.out.println();

        // Dequeue Examples
        System.out.println("=== Dequeue Demo ===");
        System.out.println("Dequeue acts as a queue with access to the top and bottom. Think of a deck of cards that you ");
        System.out.println("have access to pull or add cards to and from the top or bottom of the deck.");
        Deque<DBZCharacters> myDeque = new LinkedList<>();

        myDeque.add(new DBZCharacters("Goku", "Saiyan","Male"));
        myDeque.add(new DBZCharacters("Vegeta", "Saiyan", "Male"));

        System.out.println();
        System.out.println("Deque Contents: [" + myDeque.size() + " Objects]");
        Iterator<DBZCharacters> myDequeueIterator = myDeque.iterator();
        while (myDequeueIterator.hasNext()) {
            System.out.println(myDequeueIterator.next());
        }

        System.out.println();
        System.out.println("We will add one new entry at the front of the Deque and one at the end using addFirst and addLast");
        System.out.println();

        myDeque.addFirst(new DBZCharacters("Gohan", "Earthling-Saiyan","Male"));
        myDeque.addLast(new DBZCharacters("Frieza", "Frieza Race (mutant)", "Male"));

        System.out.println("Deque Contents: [" + myDeque.size() + " Objects]");
        myDequeueIterator = myDeque.iterator();
        while (myDequeueIterator.hasNext()) {
            System.out.println(myDequeueIterator.next());
        }
        System.out.println();

        System.out.println();
        System.out.println("We will retrieve and remove the last entry from the bottom of the Deque and then add a new");
        System.out.println(" element to top, then we will add the entry taken from the bottom to the top of the Deque.");
        System.out.println();

        DBZCharacters myDBZChar = myDeque.pollLast();
        myDeque.addFirst(new DBZCharacters("Princess Misa", "Human-type Earthling","Female"));
        myDeque.addFirst(myDBZChar);

        System.out.println("Deque Contents: [" + myDeque.size() + " Objects]");
        myDequeueIterator = myDeque.iterator();
        while (myDequeueIterator.hasNext()) {
            System.out.println(myDequeueIterator.next());
        }
        System.out.println();

        // ArrayList using Comparator
        System.out.println("=== ArrayList / Comparator Demo ===");
        System.out.println("Consists of an ArrayList full of DBZCharacters Objects that will be displayed unsorted, ");
        System.out.println("then they will be sorted using a Comparator and displayed sorted afterward.");
        System.out.println();
        ArrayList<DBZCharacters> myArrayList = new ArrayList<>();
        myArrayList.add(new DBZCharacters("Princess Misa", "Human-type Earthling","Female"));
        myArrayList.add(new DBZCharacters("Gohan", "Earthling-Saiyan","Male"));
        myArrayList.add(new DBZCharacters("Frieza", "Frieza Race (mutant)", "Male"));
        myArrayList.add(new DBZCharacters("Goku", "Saiyan","Male"));
        myArrayList.add(new DBZCharacters("Vegeta", "Saiyan", "Male"));

        System.out.println("ArrayList Contents: [" + myArrayList.size() + " Objects] (UNSORTED)");
        Iterator<DBZCharacters> myArrayIterator = myArrayList.iterator();
        while (myArrayIterator.hasNext()) {
            System.out.println(myArrayIterator.next());
        }
        System.out.println();

        // Having Collections as my class name may not be the brightest of ideas when implementing a comparator
        // learning experience, we use the complete namespace to the java.util.Collections.sort method to utilize the
        // Comparator. Good Solution to get around this, but don't make this again.
        java.util.Collections.sort(myArrayList, new DBZNameSort());

        System.out.println("ArrayList Contents: [" + myArrayList.size() + " Objects] (SORTED)");
        myArrayIterator = myArrayList.iterator();
        while (myArrayIterator.hasNext()) {
            System.out.println(myArrayIterator.next());
        }
        System.out.println();



    }
}

