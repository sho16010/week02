package mshort.week02;

import java.util.*;

public class DBZCharacters {

    private String name, gender, race;

    public  DBZCharacters(String strName, String strRace, String strGender) {
        this.name = strName;
        this.gender = strGender;
        this.race = strRace;
    }

    public  String getName() {
        return name;
    }

    public  String toString() {
        return "Name: [" + name + "] Race: [" + race + "] Gender: [" + gender + "]";
    }
}

class DBZNameSort implements Comparator<DBZCharacters>
{
    public int compare(DBZCharacters x, DBZCharacters y) {
        int result = x.getName().compareTo(y.getName());
        return result;
    }

}